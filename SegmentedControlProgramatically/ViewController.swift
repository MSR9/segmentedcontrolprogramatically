//
//  ViewController.swift
//  SegmentedControlProgramatically
//
//  Created by EPITADMBP04 on 3/25/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var cardImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        createSuitSegmentedControl()
    }

    
    func createSuitSegmentedControl() {
        let items = ["Ant","Cat","Deer","Dog","Parrot"]
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.addTarget(self, action: #selector(suitDidChnge(_:)), for: .valueChanged)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(segmentedControl)
        
        
        NSLayoutConstraint.activate([
            segmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            segmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            segmentedControl.topAnchor.constraint(equalTo: cardImageView.bottomAnchor, constant: 75)])
    }

    @objc func suitDidChnge(_ segmentedControl: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            cardImageView.image = UIImage(named: "Ant")!
        case 1:
            cardImageView.image = UIImage(named: "Cat")!
        case 2:
            cardImageView.image = UIImage(named: "Deer")!
        case 3:
            cardImageView.image = UIImage(named: "Dog")!
        default:
            cardImageView.image = UIImage(named: "Parrot")!
        }
    }
}

